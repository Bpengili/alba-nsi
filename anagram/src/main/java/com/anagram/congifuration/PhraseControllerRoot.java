package com.anagram.congifuration;


/**
 * Class holding the context root for all Phrase REST API requests and their response message .
 *
 * @author Boris Pengili
 * @since 19 Oct 2020
 */
public class PhraseControllerRoot {

    public static final String UPLOAD_FILE_URL = "/uploadfile";
    public static final String SAVE_PHRASE_URL = "/savephrase";
    public static final String GET_ANAGRAM_URL = "/getanagrams";

    public static final String FILE_SUCCESS_MESSAGE = " File was saved successfully";
    public static final String PHRASE_SUCCESS_MESSAGE = " Phrase was saved successfully";

}
