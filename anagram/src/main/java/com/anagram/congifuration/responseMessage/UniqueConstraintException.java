package com.anagram.congifuration.responseMessage;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * RuntimeException class that is thrown for unique constraint
 *
 * @author Boris Pengili
 * @since 19 Oct 2020
 */
public class UniqueConstraintException extends RuntimeException {
    private Set<String> uniqueAttributeList = new HashSet<>();
    private String message =  "";
    private final Integer statusCode = 409;

    public UniqueConstraintException(Set<String> uniqueAttribute) {
        this.uniqueAttributeList = uniqueAttribute;
        this.message = "These phrases exist already in db: ";
    }

    public Set<String> getUniqueAttribute() {
        return uniqueAttributeList;
    }

    public void setUniqueAttribute(Set<String> uniqueAttribute) {
        this.uniqueAttributeList = uniqueAttribute;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return this.message + "" + uniqueAttributeList.stream().collect(Collectors.joining(","));
    }
}
