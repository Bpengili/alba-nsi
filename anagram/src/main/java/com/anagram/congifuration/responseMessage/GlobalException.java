package com.anagram.congifuration.responseMessage;

/**
 * Exception class that is thrown for general exception (status 500)
 *
 * @author Boris Pengili
 * @since 19 Oct 2020
 */
public class GlobalException extends Exception {

    private final String message =  "Server Error";
    private final Integer statusCode = 500;

    @Override
    public String getMessage() {
        return this.message;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    @Override
    public String toString() {
        return this.message;
    }
}
