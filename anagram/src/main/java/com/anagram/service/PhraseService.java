package com.anagram.service;

import com.anagram.congifuration.responseMessage.GlobalException;
import com.anagram.congifuration.responseMessage.UniqueConstraintException;
import com.anagram.model.Phrase;
import com.anagram.service.dto.AnagramRequestDto;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Set;

/**
 * Interface class that signs basic crud operations like saving and getting on related model {@link Phrase}
 *
 * @author Boris Pengili
 * @since 19 Oct 2020
 */
@Service
public interface PhraseService {

    /**
     * Read the file line by line and save the phrase for the model {@link Phrase} for each line.
     * Before saving the phrase, firstly sort the word and save the original and the sorted one.
     * Check also if there is a violation of the unique and throw {@link UniqueConstraintException}
     *
     * @param file {@link MultipartFile} the file that is going to be saved
     * @throws IOException {@link IOException} if the file doesn't open (incorrect format exp)
     * @throws GlobalException {@link GlobalException} if something goes wrong
     */
    void saveFile(MultipartFile file) throws IOException, GlobalException;

    /**
     * The same logic as the method saveFile.
     *
     * @param phrase {@link String} the phrase that is going to be saved
     * @throws GlobalException {@link GlobalException}
     */
    void savePhrase(String phrase) throws GlobalException;

    /**
     * Find the list of anagram for the specific phrase
     * If the number of results is defined, it is searched by pageable {@link Pageable}, else it is searched normallu
     *
     * @param anagramRequestDto {@link AnagramRequestDto}
     * @return {@link Set} with {@link String} the list of anagram's result.
     */
    Set<String> getAnagrams(AnagramRequestDto anagramRequestDto);
}
