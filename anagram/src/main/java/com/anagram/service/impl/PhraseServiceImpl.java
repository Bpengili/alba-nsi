package com.anagram.service.impl;

import com.anagram.congifuration.responseMessage.GlobalException;
import com.anagram.congifuration.responseMessage.UniqueConstraintException;
import com.anagram.model.Phrase;
import com.anagram.repository.PhraseRepository;
import com.anagram.service.PhraseService;
import com.anagram.service.dto.AnagramRequestDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Service class that implements basic crud operations like saving and getting on related model {@link Phrase}
 *
 * @author Boris Pengili
 * @since 19 Oct 2020
 */
@Service
public class PhraseServiceImpl implements PhraseService {

    private static final Logger LOGGER = LogManager.getLogger(PhraseServiceImpl.class);

    @Autowired
    private PhraseRepository phraseRepository;

    private PhraseServiceImplUtil phraseServiceImplUtil = new PhraseServiceImplUtil();

    /**
     * The constructor
     */
    public PhraseServiceImpl() {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveFile(MultipartFile file) throws IOException, GlobalException {
        LOGGER.info("Saving the file");
        String line;
        Set<String> phraseException = new HashSet<>();
        InputStream is = file.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        while ((line = br.readLine()) != null) {
            Phrase phrase = phraseServiceImplUtil.createPhraseObject(line);
            try {
                phraseRepository.save(phrase);
            } catch (Exception ex) {
                if (ex.getCause() instanceof ConstraintViolationException) {
                    LOGGER.error("Error in saving the file", ex.getMessage());
                    phraseException.add(phrase.getOriginalPhrase());
                } else {
                    throw new GlobalException();
                }
            }
        }

        if (phraseException.size() > 0) {
            throw new UniqueConstraintException(phraseException);
        }
        LOGGER.info("File is saved");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void savePhrase(String phraseCharactes) throws GlobalException {
        try {
            LOGGER.info("Saving the phrase");
            Phrase phrase = phraseServiceImplUtil.createPhraseObject(phraseCharactes);
            phraseRepository.save(phrase);
        } catch (Exception ex) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                LOGGER.error("Error in saving the phrase", ex.getMessage());
                throw new UniqueConstraintException(Collections.singleton(phraseCharactes));
            }
            throw new GlobalException();
        }
        LOGGER.info("Phrase is saved");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<String> getAnagrams(AnagramRequestDto anagramRequestDto) {
        LOGGER.info("Getting Anagram for ", anagramRequestDto.getPhrase());

        String phrase = anagramRequestDto.getPhrase().toLowerCase();
        Integer limit = anagramRequestDto.getNumberOfPhrases();
        List<Phrase> phraseList = null;
        Set<String> anagramResponse = new HashSet<>();

        if (limit != -1) {
            Pageable limitResults = PageRequest.of(0, limit);
            phraseList = phraseRepository.findBySortedPhrase(phraseServiceImplUtil.sortPhraseCharacters(phrase), limitResults);
        } else {
            phraseList = phraseRepository.findBySortedPhrase(phraseServiceImplUtil.sortPhraseCharacters(phrase));
        }

        if (phraseList != null) {
            phraseList.stream().forEach(anagramFound -> anagramResponse.add(anagramFound.getOriginalPhrase()));
        }

        return anagramResponse;
    }

}
