package com.anagram.service.impl;

import com.anagram.model.Phrase;

import java.util.Arrays;

public class PhraseServiceImplUtil {

    public  PhraseServiceImplUtil(){ }

    public Phrase createPhraseObject(String phraseCharactes) {
        Phrase phrase = new Phrase();
        phrase.setOriginalPhrase(phraseCharactes.toLowerCase());
        phrase.setSortedPhrase(sortPhraseCharacters(phraseCharactes));
        return phrase;
    }

    public String sortPhraseCharacters(String line) {
        line = line.trim().replaceAll("\\s+", "").toLowerCase();
        char[] chars = line.toCharArray();
        Arrays.sort(chars);
        return new String(chars);
    }
}
