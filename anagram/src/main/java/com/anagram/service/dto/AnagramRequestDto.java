package com.anagram.service.dto;

/**
 * Object request to search for the anagram
 *
 * @author Boris Pengili
 * @since 19 Oct 2020
 */
public class AnagramRequestDto {

    private String phrase;
    private Integer numberOfPhrases;

    public AnagramRequestDto() {
    }

    public AnagramRequestDto(String phrase, Integer numberOfPhrases) {
        this.phrase = phrase;
        this.numberOfPhrases = numberOfPhrases;
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public Integer getNumberOfPhrases() {
        return numberOfPhrases;
    }

    public void setNumberOfPhrases(Integer numberOfPhrases) {
        this.numberOfPhrases = numberOfPhrases;
    }

    @Override
    public String toString() {
        return "AnagramRequestDto{" +
                "phrase='" + phrase + '\'' +
                ", numberOfPhrases=" + numberOfPhrases +
                '}';
    }
}
