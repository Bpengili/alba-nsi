package com.anagram.service.dto;
import java.util.Date;

/**
 * The responseMessage that is sent to the UI
 *
 * @author Boris Pengili
 * @since 19 Oct 2020
 */
public class ResponseMessageDto {
    private Integer statusCode;
    private Date timestamp;
    private String message;
    private String description;

    public ResponseMessageDto(Integer statusCode, String message, String description) {
        this.statusCode = statusCode;
        this.timestamp = new Date();
        this.message = message;
        this.description = description;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }

    public String getDescription() {
        return description;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "ResponseMessageDto{" +
                "statusCode=" + statusCode +
                ", timestamp=" + timestamp +
                ", message='" + message + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
