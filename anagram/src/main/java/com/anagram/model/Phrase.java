package com.anagram.model;

import javax.persistence.*;

/**
 * The persistent class for the phrase database table.
 *
 * @author Boris Pengili
 * @since 19 Oct 2020
 */
@Entity
public class Phrase {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(unique=true)
    private String originalPhrase;
    private String sortedPhrase;

    public Phrase(){

    }
    public Phrase(String originalPhrase, String sortedPhrase) {
        this.originalPhrase = originalPhrase;
        this.sortedPhrase = sortedPhrase;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOriginalPhrase() {
        return originalPhrase;
    }

    public void setOriginalPhrase(String originalPhrase) {
        this.originalPhrase = originalPhrase;
    }

    public String getSortedPhrase() {
        return sortedPhrase;
    }

    public void setSortedPhrase(String sortedPhrase) {
        this.sortedPhrase = sortedPhrase;
    }

    @Override
    public String toString() {
        return "Phrase{" +
                "id=" + id +
                ", originalPhrase='" + originalPhrase + '\'' +
                ", sortedPhrase='" + sortedPhrase + '\'' +
                '}';
    }
}
