package com.anagram.repository;

import com.anagram.model.Phrase;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The repository for the entity {@link Phrase}
 *
 * @author Boris Pengili
 * @since 19 Oct 2020
 */
@Repository
public interface PhraseRepository extends JpaRepository<Phrase, Long> {

    List<Phrase> findBySortedPhrase(String phrase, Pageable pageable);

    List<Phrase> findBySortedPhrase(String phrase);
}
