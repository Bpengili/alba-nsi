package com.anagram.controller;

import com.anagram.congifuration.responseMessage.GlobalException;
import com.anagram.service.dto.ResponseMessageDto;
import com.anagram.service.PhraseService;
import com.anagram.service.dto.AnagramRequestDto;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Set;

import static com.anagram.congifuration.PhraseControllerRoot.*;

/**
 * Rest controller class to save phrases and find anagram that is related to {@link PhraseService}
 *
 * @author Boris Pengili
 * @since 19 Oct 2020
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class PhraseController {

    /**
     * {@link PhraseService}
     */
    @Autowired
    private PhraseService phraseService;

    /**
     * The logger.
     */
    private static final Logger LOGGER = LogManager.getLogger(PhraseController.class);

    /**
     * The constructor
     */
    public PhraseController() {
    }

    /**
     * Rest method to save the file
     *
     * @param file {@link MultipartFile} The available words can be loaded from a simple text file,
     *                                   a word in each line.
     * @return {@link ResponseEntity} with {@link ResponseMessageDto} The message that file was uploaded and {@link HttpStatus} that is Ok
     * @throws IOException {@link IOException} in case there is problem to open the file
     * @throws GlobalException {@link GlobalException} general exception in case something went wrong
     */
    @PostMapping(UPLOAD_FILE_URL)
    public ResponseEntity<ResponseMessageDto> saveFile(@RequestBody MultipartFile file) throws IOException, GlobalException {
        LOGGER.info("REST request to save File : {}", file);
        phraseService.saveFile(file);
        ResponseMessageDto responseMessageDto = new ResponseMessageDto(200, FILE_SUCCESS_MESSAGE, "");
        return new ResponseEntity<ResponseMessageDto>(responseMessageDto, HttpStatus.OK);
    }

    /**
     * Rest method to save single phrase
     *
     * @param phraseCharactes {@link String} the request paramater that is going to save in db
     * @return {@link ResponseEntity} with {@link ResponseMessageDto} The message that phrase was saved and {@link HttpStatus} that is Ok
     * @throws GlobalException {@link GlobalException} general exception in case something went wrong
     */
    @GetMapping(SAVE_PHRASE_URL)
    public ResponseEntity<ResponseMessageDto> savePhrase(@RequestParam(name = "phraseCharacters") String phraseCharactes) throws GlobalException {
        LOGGER.info("REST request to save Phrase : {}", phraseCharactes);
        phraseService.savePhrase(phraseCharactes);
        ResponseMessageDto responseMessageDto = new ResponseMessageDto(200, PHRASE_SUCCESS_MESSAGE, "");
        return new ResponseEntity<>(responseMessageDto, HttpStatus.OK);
    }

    /**
     * Rest method to find anagrams for the phrase
     *
     * @param anagramRequestDto {@link AnagramRequestDto} the phrase and number of results that is needed to find
     * @return {@link ResponseEntity} with {@link Set} of {@link String} the result that is found for the anagram
     */
    @PostMapping(GET_ANAGRAM_URL)
    public ResponseEntity<Set<String>> getAnagrams(@RequestBody AnagramRequestDto anagramRequestDto) {
        LOGGER.info("REST request to find Anagrams : {}", anagramRequestDto);
        Set<String> anagramResponse = phraseService.getAnagrams(anagramRequestDto);
        return new ResponseEntity<>(anagramResponse, HttpStatus.OK);
    }
}
