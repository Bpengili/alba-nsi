package com.anagram.controller;

import com.anagram.service.dto.ResponseMessageDto;
import com.anagram.congifuration.responseMessage.GlobalException;
import com.anagram.congifuration.responseMessage.UniqueConstraintException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

/**
 * Handle exceptions across the whole application
 *
 * @author Boris Pengili
 * @since 19 Oct 2020
 */
@ControllerAdvice
public class ControllerExceptionHandler {
    private static final Logger LOGGER = LogManager.getLogger(ControllerExceptionHandler.class);

    /**
     * Handle the exception for the {@link GlobalException}
     *
     * @param ex {@link GlobalException}
     * @param request {@link WebRequest}
     * @return {@link ResponseEntity} OF {@link ResponseMessageDto}
     */
    @ExceptionHandler(GlobalException.class)
    public ResponseEntity<ResponseMessageDto> globalExceptionHandler(GlobalException ex, WebRequest request) {
        return ResponseMessageDto(ex.getStatusCode(), ex.toString(),request);
    }

    /**
     * Handle the exception for the {@link UniqueConstraintException}
     *
     * @param ex {@link GlobalException}
     * @param request {@link WebRequest}
     * @return {@link ResponseEntity} OF {@link ResponseMessageDto}
     */
    @ExceptionHandler(UniqueConstraintException.class)
    public ResponseEntity<ResponseMessageDto> UniqueConstraintExceptionHandler(UniqueConstraintException ex, WebRequest request) { ;
        return ResponseMessageDto(ex.getStatusCode(), ex.toString(),request);
    }

    /**
     * Form the responseMessageDto with right parameters
     *
     * @param statusCode - {@code int} the value of the response message
     * @param message - {@link String} the message send to the UI
     * @param request - {@link WebRequest}
     * @return {@link ResponseEntity} OF {@link ResponseMessageDto}
     */
    private ResponseEntity<ResponseMessageDto> ResponseMessageDto(int statusCode, String message, WebRequest request){
        LOGGER.error(message);
        ResponseMessageDto response = new ResponseMessageDto(
                statusCode,
                message,
                request.getDescription(false));

        return new ResponseEntity<ResponseMessageDto>(response, HttpStatus.valueOf(response.getStatusCode()));
    }



}
