package com.anagram.service;

import com.anagram.congifuration.responseMessage.GlobalException;
import com.anagram.model.Phrase;
import com.anagram.repository.PhraseRepository;
import com.anagram.service.dto.AnagramRequestDto;
import com.anagram.service.impl.PhraseServiceImpl;
import com.anagram.service.impl.PhraseServiceImplUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
public class PhraseServiceImplTest {


    @TestConfiguration
    static class PhraseServiceImplTestContextConfiguration {

        @Bean
        public PhraseService phraseServiceService() {
            return new PhraseServiceImpl();
        }
    }

    @Autowired
    private PhraseService phraseService;

    @MockBean
    private PhraseRepository phraseRepository;

    PhraseServiceImplUtil phraseServiceImplUtil = new PhraseServiceImplUtil();
    final String firstPhraseCharacters = "Test";

    @Before
    public void setUp() {
        Phrase phrase = new Phrase(firstPhraseCharacters, phraseServiceImplUtil.sortPhraseCharacters(firstPhraseCharacters));
        List<Phrase> phrases = new ArrayList<>();
        phrases.add(phrase);

        Mockito.when(phraseRepository.findBySortedPhrase(phrase.getSortedPhrase()))
                .thenReturn(phrases);
    }

    @Test
    public void whenValidSortedPhrase_thenAnagramShouldBeFound() {
        AnagramRequestDto anagramRequestDto = new AnagramRequestDto("estt",-1);
        Set<String> phrases = phraseService.getAnagrams(anagramRequestDto);

        assertThat(phrases.size())
                .isEqualTo(1);
    }

    @Test
    public void whenSaveValidFile_thenNoExceptionShouldBeThrown() {
        File file = new File("src/test/java/resources/Anagram.txt");

        try {
        InputStream in = new FileInputStream(file);
        MockMultipartFile firstFile = new MockMultipartFile("data", "filename.txt", "text/plain", in);
        phraseService.saveFile(firstFile);
        }
        catch (Exception ex){
            assertTrue("Expecting a RunTimeException here",false);
        }
    }

    @Test
    public void whenSavePhrase_thenNoExceptionShouldBeThrown() {

        try {
            phraseService.savePhrase(firstPhraseCharacters);
        }
        catch (Exception ex){
            assertTrue("Expecting a RunTimeException here",false);
        }
    }
}

