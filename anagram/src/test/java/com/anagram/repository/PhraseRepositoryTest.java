package com.anagram.repository;

import com.anagram.model.Phrase;
import com.anagram.service.impl.PhraseServiceImplUtil;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PhraseRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PhraseRepository phraseRepository;

    PhraseServiceImplUtil phraseServiceImplUtil = new PhraseServiceImplUtil();
    final String firstPhraseCharacters = "Test";
    final String secondPhraseCharacters = "etst";

    @Test
    public void whenFindBySortedPhrase_thenReturnPhrase() {
        // given
        Phrase phrase = new Phrase();
        phrase.setOriginalPhrase(firstPhraseCharacters);
        phrase.setSortedPhrase(phraseServiceImplUtil.sortPhraseCharacters(firstPhraseCharacters));
        entityManager.persist(phrase);
        entityManager.flush();

        // when
        List<Phrase> listFound = phraseRepository.findBySortedPhrase(phrase.getSortedPhrase());

        // then
        assertThat(listFound.get(0).getOriginalPhrase())
                .isEqualTo(phrase.getOriginalPhrase());
    }

    @Test
    public void whenFindBySortedPhrasePageable_thenReturnPhrase() {
        // given
        Phrase firstphrase = new Phrase(firstPhraseCharacters,phraseServiceImplUtil.sortPhraseCharacters(firstPhraseCharacters) );
        Phrase secondphrase = new Phrase(secondPhraseCharacters,phraseServiceImplUtil.sortPhraseCharacters(secondPhraseCharacters) );

        Pageable limitResults = PageRequest.of(0, 1);
        entityManager.persistAndFlush(firstphrase);
        entityManager.persistAndFlush(secondphrase);

        // when
        List<Phrase> listFound = phraseRepository.findBySortedPhrase(firstphrase.getSortedPhrase(),limitResults);

        // then
        assertThat(listFound.size())
                .isEqualTo(1);
    }

}

