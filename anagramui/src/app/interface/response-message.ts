export class ResponseMessage {
    statusCode: number;
    timestamp: Date;
    message: string = "";
    description: string;

  
}
