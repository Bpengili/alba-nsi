import { Component, OnInit } from '@angular/core';
import { AnagramService } from 'src/app/services/anagram.service';
import { AnagramTable } from 'src/app/interface/anagram-table';
import { ResponseMessage } from 'src/app/interface/response-message';
import { ToastrService } from "ngx-toastr";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@Component({
  selector: 'app-anagram',
  templateUrl: './anagram.component.html',
  styleUrls: ['./anagram.component.css']
})
export class AnagramComponent implements OnInit {

  selectedFiles: FileList;
  currentFile: File;
  phraseCharacters: string;
  anagramPhrase: string;
  checked: boolean;
  numberOfResults: number;
  anagramList: string[];
  displayedColumns: string[] = ['position', 'anagram'];
  ELEMENT_DATA: AnagramTable[] = [];
  responseMessage: ResponseMessage;
  showTable: boolean;

  constructor(private anagramService: AnagramService, private toastr: ToastrService) { }

  ngOnInit() {
    this.showTable = false;
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  //upload the file
  upload() {
    this.currentFile = this.selectedFiles.item(0);

    //show info message that file is not correct and stop the process
    if (this.currentFile.type.toString() !== "text/plain") {
      this.toastr.info("The file can not be uploaded. Only text files");
      return;
    }

    //get the response from the service
    this.anagramService.upload(this.currentFile).subscribe(
      (data: ResponseMessage) => {
        if (data.statusCode == 200)
          this.toastr.success(data.message);
      },

      (err) => {
        this.responseMessage = err.error;
        this.toastr.error(this.responseMessage.message);
      });

    this.currentFile = undefined;
  }

  //send the phrase to the backend
  submit(): void {
    this.anagramService.save(this.phraseCharacters).subscribe(
      (data: ResponseMessage) => {
        if (data.statusCode == 200) {
          this.toastr.success(data.message);
        }
      },

      (err) => {
        this.responseMessage = err.error;
        this.toastr.error(this.responseMessage.message);
      });

  }

  //search for anagram
  search(): void {
    this.ELEMENT_DATA = [];
    if (this.checked === true) {
      this.numberOfResults = -1;
    }

    this.showTable = true;
    this.anagramService.search(this.anagramPhrase, this.numberOfResults).subscribe(
      (data: string[]) => {
        
        if (data.length == 0) {
          this.toastr.info("There is not data in db with that phrase");
          return;
        }

        for (let i = 0; i < data.length; i++) {
          this.ELEMENT_DATA.push(new AnagramTable(i + 1, data[i]));
        }

      },
      (err) => {
        this.responseMessage = err.error;
        this.toastr.error(this.responseMessage.message);
      });
  }

}
