import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest,HttpParams, HttpEvent } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AnagramService {
  private baseUrl = 'http://localhost:8080';
  constructor(private http: HttpClient) { }

  upload(file: File){
    const formData: FormData = new FormData();
    formData.append('file', file);
    return this.http.post(this.baseUrl+'/uploadfile', formData);

  }

  save(phrase: string){
    let params = new HttpParams().set('phraseCharacters', phrase);
    return this.http.get(this.baseUrl+"/savephrase", { params: params});
  }

  search(anagramPhrase: string, numberOfResults: number){
    const anagramRequestDto = {phrase: anagramPhrase, numberOfPhrases: numberOfResults};
    return this.http.post(this.baseUrl+'/getanagrams', anagramRequestDto);
  }

}

